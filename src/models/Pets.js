module.exports = (sequelize, DataTypes) => {

    const Pet = sequelize.define('Pet', {
        species: {
            type: DataTypes.STRING
        },
        breed: {
            type: DataTypes.STRING
        },
        gender: {
            type: DataTypes.STRING
        },
        name: {
            type: DataTypes.STRING
        },
        age: {
            type: DataTypes.DECIMAL
        },
        price: {
            type: DataTypes.DECIMAL
        },
        picture: {
            type: DataTypes.STRING
        }
    })

    Pet.associate = function (models) {

    }
    return Pet

}
