const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const PetsController = require('./controllers/PetsController')
const multer = require('multer')
const upload = multer({dest: 'uploads/'})



module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register)
  app.post('/login',
    AuthenticationController.login)
  app.get('/pets',
    PetsController.all)
  app.post('/pets',
    upload.single('petImage'),
    PetsController.post)
  app.put('/pets',
    PetsController.put)
  app.delete('/pets', 
    PetsController.remove)
  
}