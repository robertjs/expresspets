const { Pet } = require( '../models' )


module.exports = {
    async all ( req,res,next )
    {
        try
        {
            const pets = await Pet.findAll()
            res.status( 200 ).send( pets )
        } catch ( error )
        {
            next( error )
        }
    },
    async post ( req,res )
    {
        try
        {
            const pet = await Pet.create( req.body )
            res.send( pet )
        } catch ( err )
        {
            res.status( 500 ).send( {
                error: err
            } )
        }
    },
    async put ( req,res )
    {
        try
        {
            await Pet.update( req.body,{
                where: {
                    id: req.body.id
                }
            } )
            res.send( req.body )
        } catch ( err )
        {
            res.status( 500 ).send( {
                error: 'An error occured'
            } )
        }
    },
    async remove ( req,res )
    {
        try
        {
            const PetId = await req.query.id
            const pet = await Pet.findOne( {
                where: {
                    id: PetId
                }
            } )
            await pet.destroy()
            res.status( 200 ).send( pet )

        } catch ( err )
        {
            console.log( err )
            res.status( 500 ).send( {
                error: 'An error occured'
            } )
        }
    }
}