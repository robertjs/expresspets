const {
  sequelize,
  User,
  Pet
} = require('../src/models')

const pets = require('./pets.json')

const Promise = require('bluebird')

const users = require('./users.json')


sequelize.sync({ force: true })
  .then(async function () {
    await Promise.all(
      users.map(user => {
        User.create(user)
      })
    )

    await Promise.all(
      pets.map(pet => {
        Pet.create(pet)

      })
    )
  })